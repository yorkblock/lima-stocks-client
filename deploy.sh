#!/usr/bin/env bash

set -e

npm install

expo publish --release-channel staging --non-interactive

expo build:ios --release-channel staging --no-publish

curl -o app.ipa "$(expo url:ipa --non-interactive)"

bundle exec fastlane deliver --verbose --ipa "app.ipa"
