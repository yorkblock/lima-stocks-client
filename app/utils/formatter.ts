import { Platform } from "react-native"

export const formatDecimal = (value: number | string, precision: number = 2): string => {
   if (Platform.OS === "android") return formatDecimalAndroid(value, precision)

   const decimal = typeof value === "string" ? Number(value) : value
   const formatted = decimal.toLocaleString("en-US", {
      style: "decimal",
      minimumFractionDigits: precision,
   })
   return formatted
}

export const formatAmount = (value: number | string, currency: string = "USD"): string => {
   if (Platform.OS === "android") return formatAmountAndroid(value, currency)

   const amount = typeof value === "string" ? Number(value) : value
   const locale = currency === "PEN" ? "es-PE" : "en-US"
   const formatted = amount.toLocaleString(locale, {
      style: "currency",
      currency,
      minimumFractionDigits: 2,
   })
   return formatted
}

export const formatPercentage = (value: number | string): string => {
   if (Platform.OS === "android") return formatPercentageAndroid(value)

   const percentage = typeof value === "string" ? Number(value) : value
   const formatted = (percentage / 100).toLocaleString("en-AU", {
      style: "percent",
      minimumFractionDigits: 2,
   })
   return formatted
}

//
// TODO: Find a better solution! toLocaleString doesn't work on android :-(
//
const formatDecimalAndroid = (value: number | string, precision: number = 2): string => {
   const decimal = typeof value === "string" ? Number(value) : value
   return decimal.toFixed(precision)
}

const formatAmountAndroid = (value: number | string, currency: string = "USD"): string => {
   const amount = typeof value === "string" ? Number(value) : value
   const fixed = amount.toFixed(2)

   return currency === "USD" ? "$" + fixed : "S/" + fixed
}

const formatPercentageAndroid = (value: number | string): string => {
   const percentage = typeof value === "string" ? Number(value) : value

   return "%" + (percentage / 100).toFixed(2)
}
