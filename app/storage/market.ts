import { AsyncStorage } from "react-native"
import { Market } from "../types"

const KEY = "@STG_"
const MARKET_KEY = KEY + "MARKET"

export const saveMarket = async (market: Market): Promise<void> => {
   try {
      const value = JSON.stringify(market)
      await AsyncStorage.setItem(MARKET_KEY, value)
   } catch (e) {
      console.error(e)
   }
}

export const loadMarket = async (): Promise<Market> => {
   try {
      const value = await AsyncStorage.getItem(MARKET_KEY)
      return JSON.parse(value ?? "[]")
   } catch (e) {
      console.error(e)
      return []
   }
}
