import { AsyncStorage } from "react-native"

const KEY = "@STG_"
const FAVOURITES_KEY = KEY + "FAVOURITES"

export const saveFavourites = async (nemonics: string[]): Promise<void> => {
   try {
      const value = JSON.stringify(nemonics)
      await AsyncStorage.setItem(FAVOURITES_KEY, value)
   } catch (e) {
      console.error(e)
   }
}

export const loadFavourites = async (): Promise<string[]> => {
   try {
      const value = await AsyncStorage.getItem(FAVOURITES_KEY)
      return JSON.parse(value ?? "[]")
   } catch (e) {
      console.error(e)
      return []
   }
}

export const addFavourite = async (nemonic: string): Promise<string[]> => {
   try {
      const favourites = await loadFavourites()
      if (favourites.includes(nemonic)) {
         return favourites
      }
      const newFavourites = favourites.length > 0 ? [...favourites, nemonic] : [nemonic]
      // const sorted = newFavourites.sort((s1, s2) => s1.localeCompare(s2))
      const sorted = newFavourites.sort((s1, s2) => {
         if (s1 > s2) {
            return 1
         }
         if (s1 < s2) {
            return -1
         }
         return 0
      })
      await saveFavourites(sorted)
      return sorted
   } catch (e) {
      console.error(e)
      return []
   }
}

export const deleteFavourite = async (nemonic: string): Promise<string[]> => {
   try {
      const favourites = await loadFavourites()
      const newFavourites = favourites.filter((favourite) => favourite !== nemonic)
      await saveFavourites(newFavourites)
      return newFavourites
   } catch (e) {
      console.error(e)
      return []
   }
}
