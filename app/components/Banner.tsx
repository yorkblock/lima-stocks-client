import React from "react"
import PropTypes, { InferProps } from "prop-types"
import { Platform, StyleSheet, Text, View } from "react-native"

import colors from "../config/colors"

Banner.propTypes = {
   title: PropTypes.string.isRequired,
}

type Props = InferProps<typeof Banner.propTypes>

export function Banner({ title }: Props) {
   return (
      <View style={styles.container}>
         <Text style={styles.title}>{title}</Text>
      </View>
   )
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
   },
   title: {
      color: colors.white,
      fontSize: 17,
      fontWeight: "400",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
})
