import React from "react"
import { Platform, StyleSheet, Text, View } from "react-native"

import colors from "../config/colors"

export function Welcome() {
   return (
      <View style={styles.container}>
         <Text style={styles.title}>Welcome</Text>
         <Text style={styles.body}>
            Hi there! Thanks for trying out this very unofficial viewer for the Lima Stock Exchange.
         </Text>
         <Text style={styles.body}>
            You can search by symbol, company name and sector (ex. Google, Apple, Tesla). Swipe right to add a stock to
            your favourites, swipe left to remove it.
         </Text>
         <Text style={styles.body}>The data is refreshed every hour.</Text>
      </View>
   )
}

const styles = StyleSheet.create({
   container: { flex: 1, padding: 16, justifyContent: "flex-start" },
   title: {
      color: colors.white,
      padding: 8,
      textDecorationLine: "underline",
      fontSize: 20,
      fontWeight: "800",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   body: {
      color: colors.white,
      padding: 8,
      textAlign: "justify",
      fontSize: 16,
      fontWeight: "400",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
})
