import { StyleSheet, View } from "react-native"
import React from "react"

import colors from "../config/colors"

export function Separator() {
   return <View style={styles.container} />
}

const styles = StyleSheet.create({
   container: {
      width: "100%",
      height: 1,
      backgroundColor: colors.lightGray,
   },
})
