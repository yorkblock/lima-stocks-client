import React from "react"
import Constants from "expo-constants"
import { Platform, StyleSheet, Text, View } from "react-native"

import colors from "../config/colors"

const buildNumber = (): string | number => {
   if (Platform.OS === "ios") {
      return Constants.platform?.ios?.buildNumber ?? "0"
   }

   return Constants.platform?.android?.versionCode ?? "0"
}

export function Copyright() {
   return (
      <View style={styles.container}>
         <Text style={styles.copyright}>
            {"\u00A9"} York Block 2021 v{Constants.manifest.version}({buildNumber()})
         </Text>
      </View>
   )
}

const styles = StyleSheet.create({
   container: { padding: 4 },
   copyright: {
      color: colors.white,
      fontSize: 12,
      fontWeight: "200",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
      textAlign: "center",
   },
})
