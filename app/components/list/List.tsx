import { FlatList, StyleSheet } from "react-native"
import React, { useState } from "react"

import { formatDecimal, formatPercentage } from "../../utils"
import { Item } from "./Item"
import { Separator } from "../Separator"
import { Stock } from "../../types"

type StockItem = {
   stock: Stock
   symbol: string
   name: string
   value: string
   other: string[]
   positive: boolean
}

const items = (stocks: Stock[]): StockItem[] =>
   stocks.map((stock) => {
      const { nemonico, shortName, currency, last, opening, negotiatedQuantity, percentageChange } = stock

      const formattedValue = last ? `${currency}${last}` : "---"

      const formattedStocksTraded = formatDecimal(negotiatedQuantity ?? 0, 0)

      const lastValue = Number(last)
      const openingValue = Number(opening)
      const formattedValueVariation = last && lastValue ? formatDecimal(lastValue - openingValue) : "---"

      const percentageVariation = Number(percentageChange)
      const formattedPercentageVariation = percentageVariation ? formatPercentage(percentageVariation) : "---"

      const positive = percentageVariation ? percentageVariation >= 0 : true

      return {
         stock: stock,
         symbol: nemonico,
         name: shortName,
         value: formattedValue,
         other: [formattedValueVariation, formattedPercentageVariation, formattedStocksTraded],
         positive,
      }
   })

type ListProps = {
   stocks: Stock[]
   onRefresh: () => void
   onPress: (stock: Stock) => void
   onSwipeRightEnabled: boolean
   onSwipeRight?: (stock: Stock) => void
   onSwipeLeftEnabled: boolean
   onSwipeLeft?: (stock: Stock) => void
}

export function List({
   stocks,
   onRefresh,
   onPress,
   onSwipeRightEnabled,
   onSwipeRight,
   onSwipeLeftEnabled,
   onSwipeLeft,
}: ListProps) {
   const [index, setIndex] = useState(0)

   if (stocks.length == 0) {
      return null
   }

   return (
      <>
         <FlatList
            keyboardShouldPersistTaps='always'
            style={styles.container}
            data={items(stocks)}
            keyExtractor={(stock) => stock.symbol}
            renderItem={({ item: { stock, symbol, name, value, other, positive } }: { item: StockItem }) => (
               <Item
                  symbol={symbol}
                  name={name}
                  value={value}
                  other={other[index]}
                  onPressOther={() => (index === 2 ? setIndex(0) : setIndex(index + 1))}
                  onPress={() => onPress(stock)}
                  positive={positive}
                  onSwipeableRightOpenEnabled={onSwipeRightEnabled}
                  onSwipeableRightOpen={() => (onSwipeRight ? onSwipeRight(stock) : null)}
                  onSwipeableLeftOpenEnabled={onSwipeLeftEnabled}
                  onSwipeableLeftOpen={() => (onSwipeLeft ? onSwipeLeft(stock) : null)}
               />
            )}
            ItemSeparatorComponent={Separator}
            refreshing={false}
            onRefresh={onRefresh}
            keyboardDismissMode='on-drag'
         />
      </>
   )
}

const styles = StyleSheet.create({
   container: { backgroundColor: "transparent" },
})
