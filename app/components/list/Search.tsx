import { Button, Platform, StyleSheet, TextInput, View } from "react-native"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import PropTypes, { InferProps } from "prop-types"
import React, { useRef } from "react"

import colors from "../../config/colors"

Search.propTypes = {
   value: PropTypes.string.isRequired,
   onChangeText: PropTypes.func.isRequired,
   onCancel: PropTypes.func.isRequired,
   onFocus: PropTypes.func.isRequired,
}

type Props = InferProps<typeof Search.propTypes>

export function Search({ value, onChangeText, onCancel, onFocus }: Props) {
   const textInputRef = useRef<TextInput>(null)
   const onPress = () => {
      textInputRef.current?.blur()
      onCancel()
   }

   return (
      <View style={styles.container}>
         <View style={styles.search}>
            <MaterialCommunityIcons name='magnify' size={24} color={colors.lightGray} />
            <View style={styles.textInputContainer}>
               <TextInput
                  ref={textInputRef}
                  value={value}
                  style={styles.textInput}
                  placeholder='Search'
                  placeholderTextColor={colors.lightGray}
                  clearButtonMode='while-editing'
                  autoCorrect={false}
                  autoCapitalize='none'
                  onChangeText={onChangeText}
                  onFocus={onFocus}
               />
            </View>
         </View>
         <View style={styles.cancel}>
            <Button color={colors.lightGray} title='Cancel' onPress={onPress} />
         </View>
      </View>
   )
}

const styles = StyleSheet.create({
   container: {
      flexDirection: "row",
      backgroundColor: colors.black,
      padding: 8,
   },
   search: {
      flexDirection: "row",
      flexShrink: 1,
      backgroundColor: "rgba(52, 52, 52, 0.8)",
      justifyContent: "flex-start",
      alignItems: "center",
      padding: 4,
      borderRadius: 8,
   },
   textInputContainer: {
      justifyContent: "center",
      width: "100%",
   },
   textInput: {
      color: colors.lightGray,
      fontSize: 17,
      fontWeight: "700",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   cancel: {
      marginLeft: Platform.OS === "ios" ? 0 : 8,
   },
})
