import PropTypes, { InferProps } from "prop-types"
import { Platform, StyleSheet, Text, View } from "react-native"
import React from "react"

import colors from "../../config/colors"
import { ActivityIndicator } from "../"

Header.propTypes = {
   title: PropTypes.string.isRequired,
   detail: PropTypes.string.isRequired,
   note: PropTypes.string.isRequired,
   loading: PropTypes.bool.isRequired,
}

type Props = InferProps<typeof Header.propTypes>

export function Header({ title, detail, note, loading }: Props) {
   return (
      <View style={styles.container}>
         <View style={styles.leftContainer}>
            <Text style={styles.title}>{title}</Text>
            <View style={styles.detailNoteContainer}>
               <Text style={styles.detail}>{detail}</Text>
               <Text style={styles.note}>{note}</Text>
            </View>
         </View>
         <View style={styles.rightContainer}>
            <ActivityIndicator visible={loading} />
         </View>
      </View>
   )
}

const styles = StyleSheet.create({
   container: {
      flexDirection: "row",
      paddingLeft: 8,
      paddingTop: 8,
      paddingRight: 8,
   },
   leftContainer: {
      backgroundColor: colors.black,
      padding: 0,
      flex: 1,
   },
   rightContainer: {
      width: 60,
      backgroundColor: colors.black,
   },
   title: {
      color: colors.white,
      fontSize: 28,
      fontWeight: "800",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   detailNoteContainer: {
      flexDirection: "row",
      alignItems: "baseline",
   },
   detail: {
      color: colors.gray,
      fontSize: 20,
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   note: {
      color: colors.gray,
      fontSize: 12,
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
      marginLeft: 8,
   },
})
