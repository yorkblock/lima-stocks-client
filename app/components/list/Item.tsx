import { MaterialCommunityIcons } from "@expo/vector-icons"
import PropTypes, { InferProps } from "prop-types"
import { Platform, StyleSheet, Text, TouchableHighlight, TouchableWithoutFeedback, View } from "react-native"
import React from "react"
import Swipeable from "react-native-gesture-handler/Swipeable"

import colors from "../../config/colors"

function BookmarkAction() {
   return (
      <View style={styles.bookmark}>
         <MaterialCommunityIcons name='bookmark-outline' size={30} color={colors.white} />
      </View>
   )
}

function UnbookmarAction() {
   return (
      <View style={styles.unbookmark}>
         <MaterialCommunityIcons name='trash-can-outline' size={30} color={colors.white} />
      </View>
   )
}

Item.propTypes = {
   symbol: PropTypes.string.isRequired,
   name: PropTypes.string.isRequired,
   value: PropTypes.string.isRequired,
   other: PropTypes.string.isRequired,
   onPressOther: PropTypes.func.isRequired,
   onPress: PropTypes.func.isRequired,
   positive: PropTypes.bool.isRequired,
   onSwipeableRightOpen: PropTypes.func,
   onSwipeableLeftOpen: PropTypes.func,
   onSwipeableRightOpenEnabled: PropTypes.bool.isRequired,
   onSwipeableLeftOpenEnabled: PropTypes.bool.isRequired,
}

type Props = InferProps<typeof Item.propTypes>

export function Item({
   symbol,
   name,
   value,
   other,
   onPressOther,
   onPress,
   positive,
   onSwipeableRightOpen,
   onSwipeableLeftOpen,
   onSwipeableRightOpenEnabled,
   onSwipeableLeftOpenEnabled,
}: Props) {
   return (
      <Swipeable
         renderRightActions={onSwipeableRightOpenEnabled ? UnbookmarAction : null}
         onSwipeableRightOpen={onSwipeableRightOpen}
         renderLeftActions={onSwipeableLeftOpenEnabled ? BookmarkAction : null}
         onSwipeableLeftOpen={onSwipeableLeftOpen}
      >
         <TouchableHighlight underlayColor={colors.lightGray} onPress={onPress}>
            <View style={styles.container}>
               <View style={{ width: 200 }}>
                  <Text style={styles.symbol}>{symbol}</Text>
                  <Text numberOfLines={1} style={styles.name}>
                     {name}
                  </Text>
               </View>

               <TouchableWithoutFeedback onPress={onPressOther}>
                  <View>
                     <Text style={styles.value}>{value}</Text>
                     <View
                        style={[
                           styles.otherContainer,
                           { backgroundColor: positive ? colors.positive : colors.negative },
                        ]}
                     >
                        <Text style={styles.other}>{other}</Text>
                     </View>
                  </View>
               </TouchableWithoutFeedback>
            </View>
         </TouchableHighlight>
      </Swipeable>
   )
}

const styles = StyleSheet.create({
   container: {
      backgroundColor: colors.black,
      padding: 8,
      width: "100%",
      flexDirection: "row",
      justifyContent: "space-between",
   },
   symbol: {
      color: colors.white,
      fontSize: 20,
      fontWeight: "500",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   name: {
      marginTop: 6,
      color: colors.gray,
      fontSize: 17,
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   value: {
      color: colors.white,
      textAlign: "right",
      fontSize: 17,
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   otherContainer: {
      backgroundColor: "#ff0000",
      marginTop: 4,
      padding: 4,
      borderRadius: 4,
      width: 90,
   },
   other: {
      textAlign: "right",
      color: colors.gray,
      fontSize: 14,
      fontWeight: "900",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   bookmark: {
      flex: 1,
      justifyContent: "center",
      alignItems: "flex-start",
      paddingLeft: 16,
      backgroundColor: colors.positive,
   },
   unbookmark: {
      flex: 1,
      justifyContent: "center",
      alignItems: "flex-end",
      paddingRight: 16,
      backgroundColor: colors.negative,
   },
})
