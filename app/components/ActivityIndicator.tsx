import LottieView from "lottie-react-native"
import PropTypes, { InferProps } from "prop-types"
import { ActivityIndicator as ActivityIndicatorNative, Platform } from "react-native"
import React from "react"

import colors from "../config/colors"

ActivityIndicator.propTypes = {
   visible: PropTypes.bool.isRequired,
}

type Props = InferProps<typeof ActivityIndicator.propTypes>

export function ActivityIndicator({ visible = false }: Props) {
   if (!visible) {
      return null
   }

   if (Platform.OS === "ios") return <LottieView autoPlay loop source={require("../assets/animations/loader.json")} />

   return <ActivityIndicatorNative size={50} color={colors.white} />
}
