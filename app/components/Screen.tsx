import Constants from "expo-constants"
import PropTypes, { InferProps } from "prop-types"
import React from "react"
import { SafeAreaView, StyleSheet, View } from "react-native"
import { StatusBar } from "expo-status-bar"

import colors from "../config/colors"

Screen.propTypes = {
   children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]).isRequired,
}

type ScreenProps = InferProps<typeof Screen.propTypes>

export function Screen({ children }: ScreenProps) {
   return (
      <>
         <StatusBar style='light' />
         <SafeAreaView style={styles.container}>
            <View style={{ flex: 1 }}>{children}</View>
         </SafeAreaView>
      </>
   )
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      paddingTop: Constants.statusBarHeight,
      backgroundColor: colors.primary,
   },
})
