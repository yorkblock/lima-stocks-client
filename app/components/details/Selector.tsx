import PropTypes, { InferProps } from "prop-types"
import React from "react"
import { StyleSheet, View } from "react-native"

import { Segment } from "./Segment"

Selector.propTypes = {
   selected: PropTypes.number.isRequired,
   onSelect: PropTypes.func.isRequired,
}

type SelectorProps = InferProps<typeof Selector.propTypes>

export function Selector({ selected, onSelect }: SelectorProps) {
   return (
      <View style={styles.container}>
         <Segment title='1W' selected={selected === 0} onPress={() => onSelect(0)} />
         <Segment title='1M' selected={selected === 1} onPress={() => onSelect(1)} />
         <Segment title='3M' selected={selected === 2} onPress={() => onSelect(2)} />
         <Segment title='6M' selected={selected === 3} onPress={() => onSelect(3)} />
         <Segment title='1Y' selected={selected === 4} onPress={() => onSelect(4)} />
         <Segment title='2Y' selected={selected === 5} onPress={() => onSelect(5)} />
      </View>
   )
}

const styles = StyleSheet.create({
   container: {
      flexDirection: "row",
      padding: 8,
      justifyContent: "space-evenly",
   },
})
