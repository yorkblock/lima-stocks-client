import PropTypes, { InferProps } from "prop-types"
import { Platform, Text, TouchableWithoutFeedback, StyleSheet, View } from "react-native"
import React from "react"

import colors from "../../config/colors"

Segment.propTypes = {
   title: PropTypes.string.isRequired,
   selected: PropTypes.bool.isRequired,
   onPress: PropTypes.func.isRequired,
}

type Props = InferProps<typeof Segment.propTypes>

export function Segment({ title, selected, onPress }: Props) {
   return (
      <TouchableWithoutFeedback onPress={onPress}>
         <View style={selected ? styles.segment : [styles.segment, { backgroundColor: colors.primary }]}>
            <Text style={styles.segmentText}>{title}</Text>
         </View>
      </TouchableWithoutFeedback>
   )
}

const styles = StyleSheet.create({
   segment: {
      backgroundColor: colors.lightGray,
      height: 40,
      width: 40,
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 8,
   },
   segmentText: {
      color: colors.white,
      fontSize: 17,
      fontWeight: "700",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
})
