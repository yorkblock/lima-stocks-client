import { MaterialCommunityIcons } from "@expo/vector-icons"
import PropTypes, { InferProps } from "prop-types"
import { Platform, StyleSheet, Text, TouchableWithoutFeedback, View } from "react-native"
import React from "react"

import colors from "../../config/colors"
import { Separator } from "../"

Close.propTypes = {
   onPress: PropTypes.func.isRequired,
}

type CloseProps = InferProps<typeof Close.propTypes>

function Close({ onPress }: CloseProps) {
   return (
      <View style={styles.close}>
         <TouchableWithoutFeedback onPress={onPress}>
            <MaterialCommunityIcons name='close' size={24} color={colors.white} />
         </TouchableWithoutFeedback>
      </View>
   )
}

Bookmark.propTypes = {
   isBookmarked: PropTypes.bool.isRequired,
   onPress: PropTypes.func.isRequired,
}

type BookmarkProps = InferProps<typeof Bookmark.propTypes>

function Bookmark({ isBookmarked, onPress }: BookmarkProps) {
   return (
      <View style={styles.bookmark}>
         <TouchableWithoutFeedback onPress={onPress}>
            <MaterialCommunityIcons
               name={isBookmarked ? "bookmark" : "bookmark-outline"}
               size={24}
               color={colors.white}
            />
         </TouchableWithoutFeedback>
      </View>
   )
}

Header.propTypes = {
   symbol: PropTypes.string.isRequired,
   name: PropTypes.string.isRequired,
   sector: PropTypes.string.isRequired,
   currency: PropTypes.string.isRequired,
   onClose: PropTypes.func.isRequired,
}

type Props = InferProps<typeof Header.propTypes>

export function Header({ symbol, name, sector, currency, onClose }: Props) {
   return (
      <>
         <View style={styles.container}>
            <View style={{ flexDirection: "row", alignItems: "baseline" }}>
               <Text style={styles.symbol}>{symbol}</Text>
               <Bookmark isBookmarked={false} onPress={() => console.log("Bookmark")} />
            </View>
            <Text style={styles.name}>{`${name} (${currency})`}</Text>
            {sector !== "" && <Text style={styles.sector}>{sector}</Text>}
            <Close onPress={onClose} />
         </View>
         <Separator />
      </>
   )
}

const styles = StyleSheet.create({
   container: {
      backgroundColor: colors.primary,
      padding: 8,
      marginBottom: 4,
      width: "100%",
   },
   symbol: {
      color: colors.white,
      fontSize: 28,
      fontWeight: "800",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   name: {
      color: colors.gray,
      fontSize: 16,
      fontWeight: "700",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
      marginLeft: 0,
   },
   currency: {
      color: colors.gray,
      fontSize: 14,
      fontWeight: "700",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
      marginLeft: 4,
   },
   bookmark: {
      marginLeft: 16,
   },
   sector: {
      color: colors.lightGray,
      fontSize: 14,
      fontWeight: "500",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   close: { position: "absolute", top: 8, right: 8 },
})
