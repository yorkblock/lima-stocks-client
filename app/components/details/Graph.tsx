import PropTypes, { InferProps } from "prop-types"
import React from "react"
import { Platform, StyleSheet, Text, View } from "react-native"
import { VictoryAxis, VictoryChart, VictoryLine } from "victory-native"

import colors from "../../config/colors"
import { ActivityIndicator } from "../"
import { formatDecimal } from "../../utils"

const formatTick = (tick: string, index: number, ticks: string[]): string => {
   if (index % Math.floor(ticks.length / 3) !== 0) {
      return ""
   }

   const date = new Date(tick)

   if (ticks.length <= 7) {
      return date.toLocaleDateString("en-AU", { weekday: "short" })
   }
   if (ticks.length <= 30) {
      return date.toLocaleDateString("en-AU", { month: "short", day: "numeric" })
   }
   if (ticks.length <= 90) {
      return date.toLocaleDateString("en-AU", { month: "short", day: "numeric" })
   }

   return date.toLocaleDateString("en-AU", { month: "short", day: "numeric" })
}

const lineColor = (data: { x: string; y: number }[]) => {
   if (data.length < 2) {
      return colors.white
   }

   if (data[0].y > data[data.length - 1].y) {
      return colors.negative
   }

   return colors.positive
}

const title = (data: { x: string; y: number }[]) => {
   if (data.length === 0) {
      return "No data to display"
   }

   const min = [...data].sort((a, b) => (a.y < b.y ? -1 : 1))[0]
   const max = [...data].sort((a, b) => (a.y > b.y ? -1 : 1))[0]

   return `min: ${formatDecimal(min.y)} - max: ${formatDecimal(max.y)}`
}

const domain = (data: { x: string; y: number }[]) => {
   const min = [...data].sort((a, b) => (a.y < b.y ? -1 : 1))[0]
   const max = [...data].sort((a, b) => (a.y > b.y ? -1 : 1))[0]

   const x: [number, number] = [0, data.length + 1]
   const y: [number, number] = [min.y - 1 < 0 ? 0 : min.y - 1, max.y + 1]
   return { x: x, y: y }
}

Graph.propTypes = {
   data: PropTypes.arrayOf(
      PropTypes.shape({
         x: PropTypes.string.isRequired,
         y: PropTypes.number.isRequired,
      }).isRequired,
   ).isRequired,
   loading: PropTypes.bool.isRequired,
}

type Props = InferProps<typeof Graph.propTypes>

export function Graph({ data, loading }: Props) {
   return (
      <View style={styles.container}>
         <ActivityIndicator visible={loading} />

         <Text style={styles.title}>{loading ? "" : title(data)}</Text>
         <VictoryChart domain={domain(data)}>
            <VictoryAxis dependentAxis={true} style={axis} />
            <VictoryAxis
               style={axis}
               tickFormat={(tick, index, ticks) => (loading ? "" : formatTick(tick, index, ticks))}
            />
            <VictoryLine style={{ data: { stroke: lineColor(data) } }} data={loading ? [] : (data as any)} />
         </VictoryChart>
      </View>
   )
}

const axis = {
   axis: { stroke: colors.white },
   axisLabel: { stroke: colors.white },
   tickLabels: { fill: colors.white },
}

const styles = StyleSheet.create({
   container: {},
   title: {
      color: colors.white,
      position: "absolute",
      marginTop: 15,
      alignSelf: "center",
      fontSize: 14,
      fontWeight: "700",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
})
