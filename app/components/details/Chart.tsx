import PropTypes, { InferProps } from "prop-types"
import React, { useEffect, useState } from "react"
import { StyleSheet, View } from "react-native"

import colors from "../../config/colors"
import { Graph } from "./Graph"
import { Selector } from "./Selector"
import { Separator } from "../"
import { LSX } from "../../api/lsx"
import { useApi } from "../../hooks"
import { History } from "../../types"

enum Period {
   oneWeek = 0,
   oneMonth,
   threeMonths,
   sixMonths,
   oneYear,
   twoYears,
}

const periodToDays = (period: Period): number => {
   return [7, 30, 90, 180, 365, 730][period]
}

const data = (history: History) => {
   const vals = history?.values.map((v) => {
      return { x: v[0], y: parseFloat(v[1]) }
   })

   return vals
}

Chart.propTypes = {
   symbol: PropTypes.string.isRequired,
}

type Props = InferProps<typeof Chart.propTypes>

export function Chart({ symbol }: Props) {
   const [period, setPeriod] = useState<Period>(Period.oneMonth)
   const historyApi = useApi<History>(LSX.history)

   const fetchHistoricValues = (symbol: string, period: Period) => {
      historyApi.request({ symbol: symbol, days: periodToDays(period) })
   }

   useEffect(() => {
      fetchHistoricValues(symbol, period)
   }, [period])

   return (
      <View style={styles.container}>
         <Selector
            selected={period}
            onSelect={(period) => {
               setPeriod(period)
            }}
         />
         <Separator />
         {(historyApi.data !== undefined ? true : null) && (
            <Graph data={data(historyApi.data!)} loading={historyApi.loading} />
         )}
      </View>
   )
}

const styles = StyleSheet.create({
   container: { backgroundColor: colors.primary, padding: 0 },
   graph: { backgroundColor: "rgba(255, 255, 255, .8)", borderRadius: 8, width: "100%", marginTop: 8 },
   selector: {
      flexDirection: "row",
      padding: 8,
      justifyContent: "space-evenly",
   },
})
