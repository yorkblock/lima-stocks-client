import PropTypes, { InferProps } from "prop-types"
import { Platform, StyleSheet, Text, View } from "react-native"
import React from "react"

import colors from "../../config/colors"
import { formatDecimal } from "../../utils"

Value.propTypes = {
   title: PropTypes.string.isRequired,
   detail: PropTypes.string.isRequired,
}

type ValueProps = InferProps<typeof Value.propTypes>

function Value({ title, detail }: ValueProps) {
   return (
      <View style={styles.value}>
         <Text style={styles.title}>{title}</Text>
         <Text style={styles.detail}>{detail}</Text>
      </View>
   )
}

function Separator() {
   return <View style={styles.separator} />
}

ValueGrid.propTypes = {
   open: PropTypes.number,
   last: PropTypes.number,
   date: PropTypes.string,
   current: PropTypes.number,
   sell: PropTypes.number,
   buy: PropTypes.number,
   operations: PropTypes.string,
   traded: PropTypes.string,
   amount: PropTypes.number,
}

type Props = InferProps<typeof ValueGrid.propTypes>

export function ValueGrid({ open, last, date, current, sell, buy, operations, traded, amount }: Props) {
   const formatDate = (date: string) => {
      if (date === "") {
         return "--"
      }

      const d = new Date(date)
      return d.toLocaleDateString("en-AU", { month: "short", day: "numeric" })
   }

   return (
      <View style={styles.container}>
         <View style={[styles.grid, { paddingLeft: 0 }]}>
            <Value title='Open' detail={open ? formatDecimal(open) : "--"} />
            <Separator />
            <Value title='Last' detail={last ? formatDecimal(last) : "--"} />
            <Separator />
            <Value title='Date' detail={date ? formatDate(date) : "--"} />
         </View>

         <View style={styles.grid}>
            <Value title='Curr' detail={current ? formatDecimal(current) : "--"} />
            <Separator />
            <Value title='Sell' detail={sell ? formatDecimal(sell) : "--"} />
            <Separator />
            <Value title='Buy' detail={buy ? formatDecimal(buy) : "--"} />
         </View>

         <View style={[styles.grid, { paddingRight: 0, borderRightWidth: 0 }]}>
            <Value title='Ops.' detail={operations ? formatDecimal(operations, 0) : "--"} />
            <Separator />
            <Value title='Tra.' detail={traded ? formatDecimal(traded, 0) : "--"} />
            <Separator />
            <Value title='Amt.' detail={amount ? formatDecimal(amount, 0) : "--"} />
         </View>
      </View>
   )
}

const styles = StyleSheet.create({
   container: { flexDirection: "row", paddingLeft: 8, paddingRight: 8 },
   grid: { flex: 1, paddingLeft: 8, paddingRight: 8, borderRightColor: "grey", borderRightWidth: 1 },
   value: { flexDirection: "row", justifyContent: "space-between" },
   separator: { height: 4 },
   title: {
      color: colors.lightGray,
      fontSize: 12,
      fontWeight: "700",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
   detail: {
      color: colors.white,
      fontSize: 12,
      fontWeight: "700",
      fontFamily: Platform.OS === "ios" ? "System" : "Roboto",
   },
})
