export const oneWeek = [
   {
      date: "2020-08-03",
      dayId: 1,
      value: 448.5,
   },
   {
      date: "2020-08-04",
      dayId: 2,
      value: 438.3,
   },
   {
      date: "2020-08-05",
      dayId: 3,
      value: 438.3,
   },
   {
      date: "2020-08-06",
      dayId: 4,
      value: 434.9,
   },
   {
      date: "2020-08-07",
      dayId: 5,
      value: 411.73,
   },
]

export const oneMonth = [
   {
      date: "2020-08-07",
      dayId: 1,
      value: 448.5,
   },
   {
      date: "2020-08-06",
      dayId: 2,
      value: 438.3,
   },
   {
      date: "2020-08-05",
      dayId: 3,
      value: 438.3,
   },
   {
      date: "2020-08-04",
      dayId: 4,
      value: 434.9,
   },
   {
      date: "2020-08-03",
      dayId: 5,
      value: 411.73,
   },
   {
      date: "2020-07-31",
      dayId: 6,
      value: 381.9,
   },
   {
      date: "2020-07-30",
      dayId: 7,
      value: 378.68,
   },
   {
      date: "2020-07-29",
      dayId: 8,
      value: 379.2,
   },
   {
      date: "2020-07-27",
      dayId: 9,
      value: 374.7,
   },
   {
      date: "2020-07-24",
      dayId: 10,
      value: 374.7,
   },
   {
      date: "2020-07-23",
      dayId: 11,
      value: 387.85,
   },
   {
      date: "2020-07-22",
      dayId: 12,
      value: 391.1,
   },
   {
      date: "2020-07-21",
      dayId: 13,
      value: 392.4,
   },
   {
      date: "2020-07-20",
      dayId: 14,
      value: 385.45,
   },
   {
      date: "2020-07-17",
      dayId: 15,
      value: 385.45,
   },
   {
      date: "2020-07-16",
      dayId: 16,
      value: 391.8,
   },
   {
      date: "2020-07-15",
      dayId: 17,
      value: 381.2,
   },
   {
      date: "2020-07-14",
      dayId: 18,
      value: 382.26,
   },
   {
      date: "2020-07-13",
      dayId: 19,
      value: 382.26,
   },
]

export const threeMonths = [
   {
      date: "2020-08-10",
      dayId: 1,
      value: 447,
   },
   {
      date: "2020-08-07",
      dayId: 2,
      value: 448.5,
   },
   {
      date: "2020-08-06",
      dayId: 3,
      value: 438.3,
   },
   {
      date: "2020-08-05",
      dayId: 4,
      value: 438.3,
   },
   {
      date: "2020-08-04",
      dayId: 5,
      value: 434.9,
   },
   {
      date: "2020-08-03",
      dayId: 6,
      value: 411.73,
   },
   {
      date: "2020-07-31",
      dayId: 7,
      value: 381.9,
   },
   {
      date: "2020-07-30",
      dayId: 8,
      value: 378.68,
   },
   {
      date: "2020-07-29",
      dayId: 9,
      value: 379.2,
   },
   {
      date: "2020-07-27",
      dayId: 10,
      value: 374.7,
   },
   {
      date: "2020-07-24",
      dayId: 11,
      value: 374.7,
   },
   {
      date: "2020-07-23",
      dayId: 12,
      value: 387.85,
   },
   {
      date: "2020-07-22",
      dayId: 13,
      value: 391.1,
   },
   {
      date: "2020-07-21",
      dayId: 14,
      value: 392.4,
   },
   {
      date: "2020-07-20",
      dayId: 15,
      value: 385.45,
   },
   {
      date: "2020-07-17",
      dayId: 16,
      value: 385.45,
   },
   {
      date: "2020-07-16",
      dayId: 17,
      value: 391.8,
   },
   {
      date: "2020-07-15",
      dayId: 18,
      value: 381.2,
   },
   {
      date: "2020-07-14",
      dayId: 19,
      value: 382.26,
   },
   {
      date: "2020-07-13",
      dayId: 20,
      value: 382.26,
   },
   {
      date: "2020-07-10",
      dayId: 21,
      value: 375.3,
   },
   {
      date: "2020-07-09",
      dayId: 22,
      value: 375.3,
   },
   {
      date: "2020-07-08",
      dayId: 23,
      value: 375.3,
   },
   {
      date: "2020-07-07",
      dayId: 24,
      value: 373,
   },
   {
      date: "2020-07-06",
      dayId: 25,
      value: 367.5,
   },
   {
      date: "2020-07-03",
      dayId: 26,
      value: 367.5,
   },
   {
      date: "2020-07-02",
      dayId: 27,
      value: 365.5,
   },
   {
      date: "2020-07-01",
      dayId: 28,
      value: 359.99,
   },
   {
      date: "2020-06-30",
      dayId: 29,
      value: 359.99,
   },
   {
      date: "2020-06-26",
      dayId: 30,
      value: 359.99,
   },
   {
      date: "2020-06-25",
      dayId: 31,
      value: 359.99,
   },
   {
      date: "2020-06-24",
      dayId: 32,
      value: 366.4,
   },
   {
      date: "2020-06-23",
      dayId: 33,
      value: 358.3,
   },
   {
      date: "2020-06-22",
      dayId: 34,
      value: 347.8,
   },
   {
      date: "2020-06-19",
      dayId: 35,
      value: 353.1,
   },
   {
      date: "2020-06-18",
      dayId: 36,
      value: 353.1,
   },
   {
      date: "2020-06-17",
      dayId: 37,
      value: 335,
   },
   {
      date: "2020-06-16",
      dayId: 38,
      value: 335,
   },
   {
      date: "2020-06-15",
      dayId: 39,
      value: 339.17,
   },
   {
      date: "2020-06-12",
      dayId: 40,
      value: 339.17,
   },
   {
      date: "2020-06-11",
      dayId: 41,
      value: 352.12,
   },
   {
      date: "2020-06-10",
      dayId: 42,
      value: 344.2,
   },
   {
      date: "2020-06-09",
      dayId: 43,
      value: 331.5,
   },
   {
      date: "2020-06-08",
      dayId: 44,
      value: 331.5,
   },
   {
      date: "2020-06-05",
      dayId: 45,
      value: 320.5,
   },
   {
      date: "2020-06-04",
      dayId: 46,
      value: 323.2,
   },
   {
      date: "2020-06-03",
      dayId: 47,
      value: 320.15,
   },
   {
      date: "2020-06-02",
      dayId: 48,
      value: 322,
   },
   {
      date: "2020-06-01",
      dayId: 49,
      value: 319.1,
   },
   {
      date: "2020-05-29",
      dayId: 50,
      value: 321,
   },
   {
      date: "2020-05-28",
      dayId: 51,
      value: 314.8,
   },
   {
      date: "2020-05-27",
      dayId: 52,
      value: 320,
   },
   {
      date: "2020-05-26",
      dayId: 53,
      value: 319.23,
   },
   {
      date: "2020-05-25",
      dayId: 54,
      value: 319.23,
   },
   {
      date: "2020-05-22",
      dayId: 55,
      value: 319.23,
   },
   {
      date: "2020-05-21",
      dayId: 56,
      value: 318.36,
   },
   {
      date: "2020-05-20",
      dayId: 57,
      value: 315,
   },
   {
      date: "2020-05-19",
      dayId: 58,
      value: 315,
   },
   {
      date: "2020-05-18",
      dayId: 59,
      value: 306.2,
   },
   {
      date: "2020-05-15",
      dayId: 60,
      value: 306.2,
   },
   {
      date: "2020-05-14",
      dayId: 61,
      value: 310.8,
   },
   {
      date: "2020-05-13",
      dayId: 62,
      value: 318.05,
   },
]
