export default {
   black: "#000",
   primary: "black",
   white: "#fff",
   gray: "#ccc",
   lightGray: "gray",
   positive: "forestgreen",
   negative: "orangered",
}
