import Constants from "expo-constants"

const settings = {
   dev: {
      apiUrl: Constants.manifest.extra.devApiUrl,
      apiKey: Constants.manifest.extra.devApiKey,
   },
   staging: {
      apiUrl: Constants.manifest.extra.stgApiUrl,
      apiKey: Constants.manifest.extra.stgApiKey,
   },
   prod: {
      apiUrl: Constants.manifest.extra.prdApiUrl,
      apiKey: Constants.manifest.extra.prdApiKey,
   },
}

const getCurrentSettings = () => {
   if (__DEV__) return settings.dev
   if (Constants.manifest.releaseChannel === "staging") return settings.staging
   return settings.prod
}

export default getCurrentSettings()
