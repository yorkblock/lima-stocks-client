export type Stock = {
   companyCode: string
   companyName: string
   shortName: string
   nemonico: string
   sectorCode?: string
   lastDate?: string
   previousDate?: string
   buy?: string
   sell?: string
   last?: string
   minimum?: string
   maximum?: string
   opening?: string
   previous?: string
   negotiatedQuantity?: string
   negotiatedAmount?: string
   operationsNumber?: string
   exderecho?: string
   percentageChange?: string
   currency: string
   unity?: string
   segment?: string
   createdDate: string
}
