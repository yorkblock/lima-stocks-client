export type History = {
   nemonico: string
   currencySymbol: string
   values: [string, string][]
}
