import { Stock } from "./stock"

export type Market = {
   up: number
   down: number
   equal: number
   content: Stock[]
}
