import { get, post, HttpResponse } from "./http"
import { saveMarket, loadMarket } from "../storage/market"
import { Market, History } from "../types"

export class LSX {
   static async market(): Promise<Market> {
      const url: string = "https://dataondemand.bvl.com.pe/v1/stock-quote/market"
      const body = {
         sector: "",
         today: false,
         companyCode: "",
         inputCompany: "",
      }

      try {
         const market = await LSX.post<Market>(url, body)
         if (market.content.length > 0) {
            saveMarket(market)
            return market
         }
         return loadMarket()
      } catch (e) {
         console.error(e)
         return loadMarket()
      }
   }

   static async history(p: { symbol: string; days: number }): Promise<History> {
      const startDate = new Date()
      startDate.setDate(startDate.getDate() - p.days)
      const start = startDate.toISOString().split("T")[0]

      const endDate = new Date()
      const end = endDate.toISOString().split("T")[0]

      const url: string = `https://dataondemand.bvl.com.pe/v1/stock-quote/share-values/${p.symbol}?startDate=${start}&endDate=${end}`

      return LSX.get(url)
   }

   static async post<T>(url: string, body: any): Promise<T> {
      const response: HttpResponse<T> = await post<T>(url, body)
      if (response.parsedBody === undefined) {
         return <T>{}
      }

      return response.parsedBody
   }

   static async get<T>(url: string): Promise<T> {
      const response: HttpResponse<T> = await get<T>(url)
      if (response.parsedBody === undefined) {
         return <T>{}
      }

      return response.parsedBody
   }
}
