//
// Credit: Carl Rippon (https://www.carlrippon.com/fetch-with-async-await-and-typescript/)
//

interface HttpResponse<T> extends Response {
   parsedBody?: T
}

async function http<T>(request: RequestInfo): Promise<HttpResponse<T>> {
   const response: HttpResponse<T> = await fetch(request)

   try {
      response.parsedBody = await response.json()
   } catch (ex) {}

   if (!response.ok) {
      throw new Error(response.statusText)
   }

   return response
}

async function get<T>(
   url: string,
   args: RequestInit = { method: "get", headers: { "Content-Type": "application/json" } },
): Promise<HttpResponse<T>> {
   return await http<T>(new Request(`${url}`, args))
}

async function post<T>(
   url: string,
   body: any,
   args: RequestInit = {
      method: "post",
      body: JSON.stringify(body),
      headers: { "Content-Type": "application/json" },
   },
): Promise<HttpResponse<T>> {
   return await http<T>(new Request(`${url}`, args))
}

export { get, HttpResponse, post }
