/*
 * Credit: https://stackoverflow.com/a/61127960
 */
import { useEffect, useCallback } from "react"

const useDebounce = (func: (...arg: any) => any, delay: number, deps: [any]) => {
   const callback = useCallback(func, deps)

   useEffect(() => {
      const handler = setTimeout(() => {
         callback()
      }, delay)

      return () => {
         clearTimeout(handler)
      }
   }, [delay, callback])
}

export default useDebounce
