import { useState, useRef } from "react"

const useApi = <T>(
   apiFunc: Function,
   transformFunc?: Function,
): { request: Function; loading: boolean; data: T | undefined; setData: Function; error: boolean } => {
   const [loading, setLoading] = useState(false)
   const [data, setData] = useState<T>()
   const [error, setError] = useState(false)
   const ref = useRef<number>(0)

   const request = async (params?: any) => {
      ref.current = ref.current + 1
      const key = ref.current
      setLoading(true)
      try {
         let result: T
         result = await (params ? apiFunc(params) : apiFunc())
         if (transformFunc) {
            result = await transformFunc(result)
         }

         if (key === ref.current) {
            setData(result)
            setLoading(false)
         }
      } catch (e) {
         setError(true)
         setLoading(false)
      }
   }

   return { request, loading, data, setData, error }
}

export default useApi
