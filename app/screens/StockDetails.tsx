import PropTypes, { InferProps } from "prop-types"
import React from "react"

import { Chart, Header, ValueGrid } from "../components/details"
import { Screen } from "../components"

StockDetails.propTypes = {
   stock: PropTypes.shape({
      nemonico: PropTypes.string.isRequired,
      shortName: PropTypes.string.isRequired,
      sectorDescription: PropTypes.string,
      currency: PropTypes.string.isRequired,
      opening: PropTypes.number,
      last: PropTypes.number,
      operationsNumber: PropTypes.string,
      negotiatedQuantity: PropTypes.string,
      negotiatedAmount: PropTypes.number,
      summaryLink: PropTypes.string,
      previous: PropTypes.number,
      sell: PropTypes.number,
      lastDate: PropTypes.string,
      buy: PropTypes.number,
   }),
   onClose: PropTypes.func.isRequired,
}

type Props = InferProps<typeof StockDetails.propTypes>

export function StockDetails({ onClose, stock }: Props) {
   const {
      nemonico,
      shortName,
      sectorDescription,
      currency,
      opening,
      previous,
      lastDate,
      last,
      sell,
      buy,
      operationsNumber,
      negotiatedQuantity,
      negotiatedAmount,
   } = stock ?? { nemonico: "", shortName: "", currency: "" }

   return (
      <Screen>
         <Header
            symbol={nemonico}
            name={shortName}
            sector={sectorDescription ?? ""}
            currency={currency}
            onClose={onClose}
         />

         <Chart symbol={nemonico} />

         <ValueGrid
            open={opening}
            last={previous}
            date={lastDate}
            current={last}
            sell={sell}
            buy={buy}
            operations={operationsNumber}
            traded={negotiatedQuantity}
            amount={negotiatedAmount}
         />
      </Screen>
   )
}
