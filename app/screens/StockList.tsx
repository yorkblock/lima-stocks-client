import { Modal } from "react-native"
import React, { useEffect, useState } from "react"

import { loadFavourites, addFavourite, deleteFavourite } from "../storage/favourites"
import { Header, List, Search } from "../components/list"
import { Banner, Copyright, Screen, Welcome } from "../components"
import { Market, Stock } from "../types"
import { StockDetails } from "./StockDetails"
import { useApi } from "../hooks"
import { LSX } from "../api/lsx"

const date = (stocks: Stock[]) => {
   if (stocks.length === 0) {
      return "---"
   }

   const d = new Date(stocks[0].createdDate ?? "")
   if (d instanceof Date && !isNaN(d.getTime())) {
      return d.toLocaleDateString("en-AU", { month: "long", day: "numeric" })
   }

   return "---"
}

const time = (stocks: Stock[]) => {
   if (stocks.length === 0) {
      return "--"
   }

   const d = new Date(stocks[0].createdDate ?? "")
   if (d instanceof Date && !isNaN(d.getTime())) {
      return d.toLocaleTimeString("en-AU")
   }

   return "---"
}

export default function StockList() {
   const [searchText, setSearchText] = useState("")
   const [stock, setStock] = useState<any | null>(null)
   const [favourites, setFavourites] = useState<string[]>([])

   const market = useApi<Market>(LSX.market)
   const fetchMarket = () => {
      Promise.all([loadFavourites(), market.request()]).then((value) => {
         setFavourites(value[0])
      })
   }

   const marketWithFavourites = () => {
      if (market.data === undefined) {
         return
      }
      const stocks = market.data?.content.filter((stock: Stock) => {
         return favourites.includes(stock.nemonico)
      })
      return { ...market.data, content: stocks }
   }

   const search = (text: string) => {
      const re = new RegExp(text, "i")
      return market.data?.content
         .filter((value: Stock) => value.companyName.match(re) || value.shortName.match(re) || value.nemonico.match(re))
         .filter((value: Stock) => !favourites.includes(value.nemonico))
   }

   useEffect(() => {
      fetchMarket()
   }, [])

   return (
      <>
         <Modal animationType='slide' visible={stock !== null}>
            <StockDetails stock={stock} onClose={() => setStock(null)} />
         </Modal>

         <Screen>
            <Header
               title='Stocks'
               detail={date(market.data?.content ?? [])}
               note={time(market.data?.content ?? [])}
               loading={market.loading}
            />

            <Search
               value={searchText}
               onChangeText={(text) => setSearchText(text)}
               onCancel={() => setSearchText("")}
               onFocus={() => {}}
            />

            {(searchText === "" && market.loading ? true : null) && <Banner title='Loading...' />}

            {(searchText === "" && !market.loading && favourites.length === 0 ? true : null) && <Welcome />}

            {(searchText !== "" ? true : null) && (
               <List
                  stocks={search(searchText) ?? []}
                  onRefresh={() => {}}
                  onPress={(stock) => setStock(stock)}
                  onSwipeLeftEnabled={true}
                  onSwipeRightEnabled={false}
                  onSwipeLeft={(stock) => {
                     addFavourite(stock.nemonico).then((nemonicos) => setFavourites(nemonicos))
                  }}
               />
            )}

            {(searchText === "" && favourites.length > 0 ? true : null) && (
               <List
                  stocks={marketWithFavourites()?.content ?? []}
                  onRefresh={() => fetchMarket()}
                  onPress={(stock) => setStock(stock)}
                  onSwipeLeftEnabled={false}
                  onSwipeRightEnabled={true}
                  onSwipeRight={(stock) => {
                     deleteFavourite(stock.nemonico).then((nemonicos) => setFavourites(nemonicos))
                  }}
               />
            )}

            <Copyright />
         </Screen>
      </>
   )
}
