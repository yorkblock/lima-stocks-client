import React from "react"

import StockList from "./app/screens/StockList"

export default function App() {
   return <StockList />
}
