import { ConfigContext, ExpoConfig } from "@expo/config"

export default ({ config }: ExpoConfig): ConfigContext => ({
   ...config,
   extra: {
      devApiKey: process.env.LSX_API_KEY,
      devApiUrl: process.env.LSX_API_URL_DEV,
      stgApiKey: process.env.LSX_API_KEY,
      stgApiUrl: process.env.LSX_API_URL_STG,
      prdApiKey: process.env.LSX_API_KEY,
      prdApiUrl: process.env.LSX_API_URL_PRD,
   },
})
